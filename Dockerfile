# Use latest Alpine based ruby image
FROM ruby:alpine

# Take HTMLProofer version as an argument
ARG HTMLPROOFER_VERSION

# Add HTMLProofer
RUN test -n "${HTMLPROOFER_VERSION}" || { echo "Please provide a HTMLProofer version!"; exit 1; } && \
    apk add --no-cache libcurl xz-libs && \
    apk add --no-cache --virtual build-dependencies build-base libxml2-dev libxslt-dev && \
    gem install html-proofer --version "= $HTMLPROOFER_VERSION" --no-document && \
    echo -ne "#!/usr/bin/env sh\nhtmlproofer ./public/ --checks Links,Images,Scripts,Favicon --check_sri --check_external_hash --enforce-https --ignore-status-codes 503 --allow_hash_href --ignore-files ./public/404.html\n" > /bin/htmlproofer-complete && \
    chmod +x /bin/htmlproofer-complete && \
    apk del build-dependencies && \
    find /tmp -mindepth 1 -maxdepth 1 | xargs rm -rf
