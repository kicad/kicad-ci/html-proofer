# HTML Proofer

[![pipeline status](https://gitlab.com/kicad/kicad-ci/html-proofer/badges/main/pipeline.svg)](https://gitlab.com/kicad/kicad-ci/html-proofer/-/commits/main)

[HTMLProofer](https://github.com/gjtorikian/html-proofer) docker image, mainly for HTML quality testing.
